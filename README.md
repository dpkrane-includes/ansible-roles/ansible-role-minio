## Роль раскатывает на хостах (с помощью роли [ansible-role-swarm](https://gitlab.com/dpkrane-includes/ansible-roles/ansible-role-swarm)) S3-совместимое хранилище Minio.

```
minio_data_dir: "/opt/minio/data"
minio_access_key: "qwerty"
minio_secret_key: "asdfasdf"
minio_config: "/opt/minio/minio.yaml"

```
